#!/usr/bin/env bash
# set -x

PWD=$(pwd)
app=$(basename $PWD)
version=$(grep 'version =' ${app}/default/app.conf | awk '{print $3}' | sed 's/\.//g')

exclude_opts=""
[ -d "${app}/local" ] && exclude_opts+="--exclude=${app}/local "
[ -f "${app}/metadata/local.meta" ] && exclude_opts+="--exclude=${app}/metadata/local.meta "

tar -czf ${app}_${version}.tgz ${app} ${exclude_opts}
echo "Wrote: ${app}_${version}.tgz"

exit 0
